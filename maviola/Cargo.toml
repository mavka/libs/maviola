[package]
name = "maviola"
description = "High-level MAVLink communication library with support for essential micro-services."
version = "0.3.0-rc.1"
edition = "2021"
authors = ["Mykhailo Ziatin <ziatin@mathwithoutend.org>"]
repository = "https://gitlab.com/mavka/libs/maviola"
readme = "../README.md"
license = "MIT OR Apache-2.0"
homepage = "https://gitlab.com/mavka/libs/maviola"
keywords = ["MAVLink", "UAV", "drones"]
categories = [
    "aerospace::protocols",
    "aerospace::drones",
    "aerospace::unmanned-aerial-vehicles",
]
resolver = "2"
include = [
    "src",
    "Cargo.toml"
]

[lib]
name = "maviola"
path = "src/lib.rs"

###########################################################
# Dependencies
###########################################################
[dependencies]
log = "0.4.21"
portpicker = "0.1.1"
serde = { version = "1.0.197", default-features = false, features = ["derive"], optional = true }
serde_arrays = { version = "0.1.0", default-features = false, optional = true }
specta = { version = "=2.0.0-rc.22", features = ["derive"], optional = true }
specta-util = { version = "0.0.9", optional = true }
thiserror = "2.0.9"
serialport = { version = "4.6.1", optional = true }

# Mavka
mavio = { version = "0.5.4", features = ["extras", "dlct-minimal", "sha2", "std"] }

# Async dependencies
async-stream = { version = "0.3.5", optional = true }
async-trait = { version = "0.1.79", optional = true }
tokio = { version = "1.36.0", default-features = false, features = ["sync", "rt", "net", "fs", "io-util", "time"], optional = true }
tokio-stream = { version = "0.1.15", features = ["sync"], optional = true }
tokio-util = { version = "0.7.10", optional = true }
tokio-serial = { version = "5.4.4", optional = true }

# Documentation
document-features = "0.2.10"

[dev-dependencies]
env_logger = "0.11.3"
tokio = { version = "1.36.0", default-features = false, features = ["sync", "rt", "rt-multi-thread", "net", "fs", "io-util", "time", "test-util", "macros"] }

###########################################################
# Features
###########################################################
[features]
#==========================================================
#! ## Generic features
#==========================================================

## Default features (empty).
default = []

## All stable features (no unsafe features).
## This should be used instead of --all-features for most of the production environments
full = [
    "derive",
    "sync",
    "async",
    "serde",
    "specta",
    "dlct-all",
    "msrv-all",
    "definitions",
]

## Enables unstable API features.
unstable = [
    "mavio/unstable"
]
## Unsafe features.
unsafe = [
    "mavio/unsafe"
]

#==========================================================
#! ## MAVSpec tools
#==========================================================

## Includes derive maros from MAVSpec
derive = ["mavio/derive"]

## Includes MAVLink message definitions
definitions = [
    "mavio/definitions",
]

#==========================================================
#! ## I/O
#==========================================================

## Enables synchronous API.
sync = [
    "dep:serialport",
]

## Enables asynchromous API via Tokio.
async = [
    "dep:async-stream",
    "dep:async-trait",
    "dep:tokio",
    "dep:tokio-stream",
    "dep:tokio-util",
    "dep:tokio-serial",
    "mavio/tokio"
]

#==========================================================
#! ## Serialization and reflection
#==========================================================
#! These features enable [serde](https://serde.rs) and [specta](https://specta.dev) support.

## Enables serde support.
serde = [
    "dep:serde",
    "dep:serde_arrays",
    "mavio/serde",
]
## Enables specta support.
specta = [
    "dep:specta",
    "dep:specta-util",
    "mavio/specta",
]

#==========================================================
#! ## Dialects
#==========================================================
#! Bundle standard MAVLink dialects as defined in XML
#! [message definitions](https://gitlab.com/mavka/spec/protocols/mavlink/message-definitions-v1.0) generated by
#! [MAVSpec](https://crates.io/crates/mavspec).

## Include `ardupilotmega` dialect
dlct-ardupilotmega = ["dlct-common", "mavio/dlct-ardupilotmega"]
## Include `ASLUAV` dialect
dlct-asluav = ["mavio/dlct-asluav"]
## Include `AVSSUAS` dialect
dlct-avssuas = ["dlct-common", "mavio/dlct-avssuas"]
## Include `common` dialect
dlct-common = ["dlct-minimal", "mavio/dlct-common"]
## Include `csAirLink` dialect
dlct-cs_air_link = ["mavio/dlct-cs_air_link"]
## Include `cubepilot` dialect
dlct-cubepilot = ["dlct-common", "mavio/dlct-cubepilot"]
## Include `development` dialect
dlct-development = ["dlct-common", "mavio/dlct-development"]
## Include `icarous` dialect
dlct-icarous = ["mavio/dlct-icarous"]
## Include `matrixpilot` dialect
dlct-matrixpilot = ["dlct-common", "mavio/dlct-matrixpilot"]
## Include `minimal` dialect
dlct-minimal = ["mavio/dlct-minimal"]
## Include `paparazzi` dialect
dlct-paparazzi = ["dlct-common", "mavio/dlct-paparazzi"]
## Include `standard` dialect
dlct-standard = ["dlct-minimal", "mavio/dlct-standard"]
## Include `ualberta` dialect
dlct-ualberta = ["dlct-common", "mavio/dlct-ualberta"]
## Include `uAvionix` dialect
dlct-uavionix = ["dlct-common", "mavio/dlct-uavionix"]

## Include `all` meta-dialect
dlct-all = [
    "dlct-ardupilotmega",
    "dlct-common",
    "dlct-asluav",
    "dlct-avssuas",
    "dlct-cs_air_link",
    "dlct-cubepilot",
    "dlct-development",
    "dlct-icarous",
    "dlct-matrixpilot",
    "dlct-minimal",
    "dlct-paparazzi",
    "dlct-standard",
    "dlct-ualberta",
    "dlct-uavionix",
    "mavio/dlct-all"
]

#==========================================================
#! ## MAVLink microservices
#==========================================================
#! These features will control generation of MAVLink microservice-specific bindings.

## Support for all MavLink microservices
msrv-all = [
    "msrv-heartbeat",
    "msrv-mission",
    "msrv-parameter",
    "msrv-parameter-ext",
    "msrv-command",
    "msrv-manual-control",
    "msrv-camera",
    "msrv-gimbal-v1",
    "msrv-gimbal-v2",
    "msrv-arm-auth",
    "msrv-image-transmission",
    "msrv-ftp",
    "msrv-landing-target",
    "msrv-ping",
    "msrv-path-planning",
    "msrv-battery",
    "msrv-terrain",
    "msrv-tunnel",
    "msrv-open-drone-id",
    "msrv-high-latency",
    "msrv-component-metadata",
    "msrv-payload",
    "msrv-traffic-management",
    "msrv-events-interface",
    "msrv-time-sync",
    "mavio/msrv-all",
]
## Heartbeat protocol support
msrv-heartbeat = ["msrv", "mavio/msrv-heartbeat"]
## Mission protocol support
msrv-mission = ["msrv", "mavio/msrv-mission"]
## Parameter protocol support
msrv-parameter = ["msrv", "mavio/msrv-parameter"]
## Extended parameter protocol support
msrv-parameter-ext = ["msrv", "mavio/msrv-parameter-ext"]
## Command protocol support
msrv-command = ["msrv", "mavio/msrv-command"]
## Manual control protocol support
msrv-manual-control = ["msrv", "mavio/msrv-manual-control"]
## Camera protocol v2 support
msrv-camera = ["msrv", "mavio/msrv-camera"]
## Gimbal protocol v1 support
msrv-gimbal-v1 = ["msrv", "mavio/msrv-gimbal-v1"]
## Gimbal protocol v2 support
msrv-gimbal-v2 = ["msrv", "mavio/msrv-gimbal-v2"]
## Arm authorization protocol support
msrv-arm-auth = ["msrv", "mavio/msrv-arm-auth"]
## Image transmission protocol support
msrv-image-transmission = ["msrv", "mavio/msrv-image-transmission"]
## File transfer protocol support
msrv-ftp = ["msrv", "mavio/msrv-ftp"]
## Landing target protocol support
msrv-landing-target = ["msrv", "mavio/msrv-landing-target"]
## Ping protocol support
msrv-ping = ["msrv", "mavio/msrv-ping"]
## Path planning protocol support
msrv-path-planning = ["msrv", "mavio/msrv-path-planning"]
## Battery protocol support
msrv-battery = ["msrv", "mavio/msrv-battery"]
## Terrain protocol support
msrv-terrain = ["msrv", "mavio/msrv-terrain"]
## Tunnel protocol support
msrv-tunnel = ["msrv", "mavio/msrv-tunnel"]
## Open Drone ID protocol support
msrv-open-drone-id = ["msrv", "mavio/msrv-open-drone-id"]
## High latency protocol support
msrv-high-latency = ["msrv", "mavio/msrv-high-latency"]
## Component metadata protocol support
msrv-component-metadata = ["msrv", "mavio/msrv-component-metadata"]
## Payload protocol support
msrv-payload = ["msrv", "mavio/msrv-payload"]
## Traffic management protocol support
msrv-traffic-management = ["msrv", "mavio/msrv-traffic-management"]
## Events interface protocol support
msrv-events-interface = ["msrv", "mavio/msrv-events-interface"]
## Time synchronization protocol support
msrv-time-sync = ["msrv", "mavio/msrv-time-sync"]

#==========================================================
#! ## Additional MAVLink tools
#==========================================================
#! These features will enable additional MAVLink utilities such as `*.waypoints` files support, mission planninc, etc.
#!
#! ⚠️ All such features require `unstable` feature to be enabled in order to take effect.

## All MAVLink microservices utils
##
## ⚠️ Requires `unstable` feature to take effect.
msrv-utils-all = [
    "msrv-utils-mission",
    "mavio/msrv-utils-all",
]
## Mission protocol utils
##
## ⚠️ Requires `unstable` feature to take effect.
msrv-utils-mission = [
    "msrv-utils",
    "msrv-mission",
    "dlct-common",
    "mavio/msrv-utils-mission",
]

#==========================================================
#! ## Technical features
#==========================================================
#! These features should not be used directly.

## ⊛ Enable MAVLink microservices support
##
## Do not use directly as this feature does not give access to any specific functionality by itself. Instead, use one of
## `msrv-*` features.
msrv = [
    "mavio/msrv",
]

## ⊛️ Enables MAVLink microservices extra utils
##
## Do not use directly as this feature does not give access to any specific functionality by itself. Instead, use one of
## `msrv-utils-*` features.
msrv-utils = [
    "msrv",
    "mavio/msrv-utils",
]

#==========================================================
#! ## Test utils (⚠️ do not use at production ⚠️)
#==========================================================

## Add testing utils that allow to run complex tests.
## Primarily used for documentation but can be utilized by
## other libraries as well.
test_utils = [
    "derive",
    "sync",
    "async",
    "unstable",
    "unsafe",
]

###########################################################
# Examples
###########################################################
[[example]]
name = "tcp_ping_pong"
test = true
required-features = ["sync"]

[[example]]
name = "udp_ping_pong"
test = true
required-features = ["sync"]

[[example]]
name = "sock_ping_pong"
test = true
required-features = ["sync"]

[[example]]
name = "file_rw"
test = true
required-features = ["sync"]

[[example]]
name = "serial_fc"
test = false
required-features = ["sync", "dlct-common"]

[[example]]
name = "message_signing"
test = true
required-features = ["sync"]

[[example]]
name = "network"
test = true
required-features = ["sync"]

[[example]]
name = "multiple_devices"
test = true
required-features = ["sync"]

[[example]]
name = "scrambler"
test = true
required-features = ["sync", "unsafe"]

[[example]]
name = "async_tcp_ping_pong"
test = true
required-features = ["async"]

[[example]]
name = "async_udp_ping_pong"
test = true
required-features = ["async"]

[[example]]
name = "async_sock_ping_pong"
test = true
required-features = ["async"]

[[example]]
name = "async_file_rw"
test = true
required-features = ["async"]

[[example]]
name = "async_serial_fc"
test = false
required-features = ["async", "dlct-common"]

[[example]]
name = "async_network"
test = true
required-features = ["async"]

[[example]]
name = "async_multiple_devices"
test = true
required-features = ["async"]

###########################################################
# Linter
###########################################################
[lints.rust]
unexpected_cfgs = { level = "allow", check-cfg = ['cfg(rustdoc)'] }

###########################################################
# Metadata
###########################################################
[package.metadata.docs.rs]
all-features = true
rustdoc-args = ["--cfg", "docsrs"]
